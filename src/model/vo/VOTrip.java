package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	
	private int id;
	
	private double seconds;
	
	private String stationName;
	
	private String destinationName;
	
	private String gender;
	
	private int fromId;
	
	private int toId;

	public VOTrip(int id, double seconds, String stationName, String destinationName, String gender, int fromId, int toId)
	{
		this.id = id;
		this.seconds = seconds;
		this.stationName = stationName;
		this.destinationName = destinationName;
		this.gender = gender;
		this.fromId = fromId;
		this.toId = toId;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return seconds;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return stationName;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return destinationName;
	}
	
	public String getGender()
	{
		return gender;
	}
	
	public int getFromId()
	{
		return fromId;
	}
	
	public int getToId()
	{
		return toId;
	}
}
