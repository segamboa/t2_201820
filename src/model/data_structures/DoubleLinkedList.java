package model.data_structures;

import java.util.Iterator;
import java.util.ListIterator;





public class DoubleLinkedList<T> implements DoublyLinkedList<T> {

	private Node<T> primerNodo;
	
	private int cantidadElem;
	
	public DoubleLinkedList()
	{
		primerNodo = null;
		cantidadElem = 0;			
	}
	
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorLista<T>(primerNodo);
	}

	public void add(T elemento) {
		// TODO Auto-generated method stub
		Node<T> agregar = new Node<T>( elemento );
        if( primerNodo == null )
        {
            primerNodo = agregar;
        }
        else
        {
            primerNodo.cambiarAnterior(agregar);
            agregar.cambiarSiguiente(primerNodo);
            primerNodo = agregar;
        }
        cantidadElem++;
		
	}

	public void addAtK(T elemento, int index) {
		// TODO Auto-generated method stub
		if(index < 0 || index > getSize())
		{
			throw new IndexOutOfBoundsException("El indice es mayor que el tama�o o menor que cero");
		}
		
		Node<T> actual = primerNodo;
		
		Node<T> agregar = new Node<T>(elemento);
		
		for(int i = 0; i < index - 1; i++)
		{
			actual = actual.darSiguiente();
		}
		agregar.cambiarAnterior(actual);
        agregar.cambiarSiguiente(actual.darSiguiente());
        actual.darSiguiente().cambiarAnterior(agregar);
        actual.cambiarSiguiente(agregar);
		cantidadElem++;
		
	}

	public T getElement(int pos) {
		// TODO Auto-generated method stub
		
		Node<T> actual = primerNodo;
		int pos1 = 0;
		if(pos < 0 || pos > getSize())
		{
			throw new IndexOutOfBoundsException("El indice es mayor que el tama�o o menor que cero");
		}
		while(pos != pos1 && actual.darSiguiente() != null)
		{
			actual = actual.darSiguiente();
			pos1++;
		}
		if(pos1 == pos)
		{
			return actual.darElemento();
		}
		
		return null;
	}

	public T getCurrentElement() {
		// TODO Auto-generated method stub
		return primerNodo.darElemento();
	}

	public int getSize() {
		// TODO Auto-generated method stub
		return cantidadElem;
	}

	public void delete() {
		// TODO Auto-generated method stub
		Node<T> actual = primerNodo;
        if(primerNodo != null)
        {
        	if(actual.darSiguiente() != null)
        	{
            actual = actual.darSiguiente();            
            primerNodo = actual;
            actual.cambiarAnterior(null);
            cantidadElem--;
        	}
        	else
        	{
        		primerNodo = null;
        		cantidadElem--;
        	}
        }
		
		
	}

	public void addAtEnd(T elemento) {
		// TODO Auto-generated method stub
		Node<T> actual = primerNodo;
		Node<T> agregar = new Node<T>(elemento);
		if(primerNodo != null)
		{

			while(actual.darSiguiente() != null )
			{
				actual = actual.darSiguiente();
			}
			if(actual.darSiguiente() == null)
			{
				actual.cambiarSiguiente(agregar);
				agregar.cambiarAnterior(actual);
				cantidadElem++;

			}
		}
		
	}

	public void deleteAtK( int index) {
		// TODO Auto-generated method stub
		if(index < 0 || index > getSize())
		{
			throw new IndexOutOfBoundsException("El indice es mayor que el tama�o o menor que cero");
		}

		Node<T> actual = primerNodo;	
		int contador = 0;
		if(primerNodo != null)
		{

			while(actual.darSiguiente() != null && index != contador)
			{
				actual = actual.darSiguiente();
				contador++;
			}
			if(contador == index)
			{
	
				actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
				actual.darSiguiente().cambiarAnterior(actual.darAnterior());
				cantidadElem--;
			}
		}
		
	}

	public Node<T> next(Node<T> nodo) {
		// TODO Auto-generated method stub
		if(nodo.darSiguiente() != null)
		{
		return nodo.darSiguiente();
		}
		else
		{
			return null;
		}
	}

	public Node<T> previous(Node<T> nodo) {
		// TODO Auto-generated method stub
		if(nodo.darAnterior() != null)
		{
		return nodo.darAnterior();
		}
		else
		{
			return null;
		}
	}


}
