package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T>  {
	
	public void add(T elemento);
	
	public void addAtK(T elemento, int index);
	
	public T getElement(int pos);
	
	public T getCurrentElement();
	
	public int getSize();
	
	public void delete();
	
	public void addAtEnd(T elemento);
	
	public void deleteAtK( int index);
	
	public Node<T> next(Node<T> nodo);
	
	public Node<T> previous(Node<T> nodo);
	
	
}
