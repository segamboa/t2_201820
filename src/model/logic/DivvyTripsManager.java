package model.logic;

import java.io.FileReader;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStations;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IteradorLista;

public class DivvyTripsManager implements IDivvyTripsManager  {

	
	DoubleLinkedList<VOTrip> trip = new DoubleLinkedList<VOTrip>();
	
	DoubleLinkedList<VOStations> stations = new DoubleLinkedList<VOStations>();
	
	
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		try
		{
		CSVReader reader = new CSVReader(new FileReader(stationsFile));
	     String [] nextLine;
	     nextLine = reader.readNext();
	     while ((nextLine = reader.readNext()) != null) {
	        // nextLine[] is an array of values from the line
	    	 VOStations viaje = new VOStations(Integer.parseInt(nextLine[0]),nextLine[1], nextLine[2], Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Integer.parseInt(nextLine[5]));
	        stations.add(viaje);
	     }
	     reader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		try
		{
		CSVReader reader = new CSVReader(new FileReader(tripsFile));
	     String [] nextLine;
	     nextLine = reader.readNext();
	     while ((nextLine = reader.readNext()) != null) {
	        // nextLine[] is an array of values from the line
	    	 VOTrip viaje = new VOTrip(Integer.parseInt(nextLine[0]),Double.parseDouble(nextLine[4]), nextLine[6], nextLine[8], nextLine[10], Integer.parseInt(nextLine[5]), Integer.parseInt(nextLine[7]));
	        trip.add(viaje);
	     }
	     reader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub
		DoubleLinkedList<VOTrip> genero = new DoubleLinkedList<VOTrip>();
		for(VOTrip genders: trip)
        {
            if(genders.getGender().equals(gender))
            {
                genero.add(genders);
            }
        }
        return genero;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		DoubleLinkedList<VOTrip> estacion = new DoubleLinkedList<>();
		for(VOTrip estaciones: trip)
		{
			if(estaciones.getToId() == stationID)
			{
				estacion.add(estaciones);
			}
		}
		return estacion;
	}	


}
