package model.vo;

public class VOStations 
{

	private int id;
	
	private String name;
	
	private int capacity;
	
	private String city;
	
	private double latitude;
	
	private double longitude;

	public VOStations(int id, String name, String city, double latitude, double longitude, int capacity)
	{
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.capacity = capacity;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getCapacity()
	{
		return capacity;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public double getLatitude()
	{
		return latitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
}
