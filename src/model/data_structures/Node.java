package model.data_structures;


public class Node<T> {
	/**
	 * Constante de serialización
	 */
	private static final long serialVersionUID = 1L;
	
	protected T elemento;

	/**
	 * Nodo anterior.
	 */
	private Node<T> anterior;
	
	private Node<T> siguiente;

	/**
	 * Método constructor del nodo doblemente encadenado
	 * @param elemento elemento que se almacenará en el nodo.
	 */
	public Node(T elemento) 
	{
		this.elemento = elemento;
		siguiente = null;
	}

	/**
	 * Método que retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public Node<T> darAnterior()
	{
		return anterior;
	}
	
	public Node<T> darSiguiente()
	{
		return siguiente;
	}

	/**
	 * Método que cambia el nodo anterior por el que llega como parámetro.
	 * @param anterior Nuevo nodo anterior.
	 */
	public void cambiarAnterior(Node<T> anterior)
	{
		this.anterior = anterior;
	}
	
	public void cambiarSiguiente(Node<T> siguiente)
	{
		this.siguiente = siguiente;
	}
	
	public T darElemento()
	{
		return elemento;
	}
	
	public void cambiarElemento(T elemento)
	{
		this.elemento = elemento;
	}
}
