package model.data_structures;

import java.io.Serializable;
import java.util.ListIterator;

/**
 * Clase que representa el iterador de lista (avanza hacia adelante y hacia atr�s)
 * @param <E> Tipo del objeto que almacena el iterador de la lista
 */
public class IteradorLista<T> implements ListIterator<T>, Serializable 
{

	/**
	 * Constante de serializaci�n
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Nodo anterior al que se encuentra el iterador.
	 */
	private Node<T> anterior;

	/**
	 * Nodo en el que se encuentra el iterador.
	 */
	private Node<T> actual;
	
	/**
     * Crea un nuevo iterador de lista iniciando en el nodo que llega por par�metro
     * @param primerNodo el nodo en el que inicia el iterador. nActual != null
     */
	public IteradorLista(Node<T> primerNodo)
	{
		actual = primerNodo;
		anterior = null;
	}
	
    /**
     * Indica si hay nodo siguiente
     * true en caso que haya nodo siguiente o false en caso contrario
     */
	public boolean hasNext() 
	{
		// TODO Completar seg�n la documentaci�n
		boolean retornar = false;
		if(actual.darSiguiente() != null)
		{
			retornar = true;
		}
		return retornar;
	}

    /**
     * Indica si hay nodo anterior
     * true en caso que haya nodo anterior o false en caso contrario
     */
	public boolean hasPrevious() 
	{
		// TODO Completar seg�n la documentaci�n
		boolean retornar = false;
		if(anterior != null)
		{
			retornar = true;
		}
		return retornar;
	}

    /**
     * Devuelve el elemento siguiente de la iteraci�n y avanza.
     * @return elemento siguiente de la iteraci�n
     */
	public T next() 
	{
		// TODO Completar seg�n la documentaci�n
		actual = actual.darSiguiente();
		return actual.darElemento();
	}

    /**
     * Devuelve el elemento anterior de la iteraci�n y retrocede.
     * @return elemento anterior de la iteraci�n.
     */
	public T previous() 
	{
		// TODO Completar seg�n la documentaci�n
		actual = actual.darAnterior();
		return actual.darElemento();
		
	}
	
	
	//=======================================================
	// M�todos que no se implementar�n
	//=======================================================
	
	public int nextIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public int previousIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public void remove() 
	{
		throw new UnsupportedOperationException();
	}

	public void set(T e) 
	{
		throw new UnsupportedOperationException();
	}
	
	public void add(T e) 
	{
		throw new UnsupportedOperationException();		
	}

}
