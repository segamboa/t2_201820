package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.DoubleLinkedList;


public class AddTest 
{

	private DoubleLinkedList<Integer> lista;

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Prepara los datos de prueba del escenario1
	 */
	@Before
	public void setupEscenario1( )
	{
		lista = new DoubleLinkedList<Integer>();
		lista.add(12);
		lista.add(13);
		lista.add(123);
	}
	
	public void setupEscenario2( )
	{
		lista.addAtK(11, 2);
		lista.addAtEnd(1);	
	}
	
	public void setupEscenario3( )
	{		
		lista.deleteAtK(1);
	}
	
	public void setupEscenario4()
	{
		lista.delete();			
	}

	/**
	 * Prueba que los datos b�sicos se recuperen adecuadamente
	 */
	@Test
	public void tests( )
	{
		setupEscenario1();
		//Prueba metodo para a�adir al inicio
		assertTrue("La lista esta a�adiendo erroneamente", lista.getSize() == 3);
		
		setupEscenario2();
		//Prueba metodos para a�adir al final y en una posicion determinada
		assertTrue("El elemento no se a�adio en la posicion indicada", lista.getElement(2).equals(11));
		assertTrue("El elemento no se a�adio al final", lista.getElement(3).equals(12));
		assertTrue("El elemento solicitado es diferente", lista.getSize() == 5);
		
		setupEscenario3();	
		//Prueba de metodo para borrar un elemento en una posicion indicada
		assertTrue("El elemento se borr� mal", lista.getElement(1).equals(11));
		
		setupEscenario4();
		//Prueba de metodo de borrar el primer elemento de la lista y del metodo getElement()
		assertTrue("El elemento se borr� mal1", lista.getElement(0).equals(11));
		assertTrue("El elemento buscado no es el correcto", lista.getElement(1).equals(12));
		
	}
}
